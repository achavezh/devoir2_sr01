#include "explorer.h"
#include "premier.h"
#include "my_system.h"

void explorer(int debut, int fin){
  int etat,pid,pid2;
  pid = fork();
  if(pid==0){
    for (int i=debut; i<=fin;i++){

      if (premier(i)==1) {
        pid2=fork();
        fflush(stdout);
        if (pid2==0){
          char chaine[100];
          sprintf(chaine,"%d  est un nombre premier écrit par le processus %d, son pere est le processus %d et le PID du processus systeme est %d",i,getpid(), getppid(), getpid()+1);
          my_system(chaine);
          sleep(2);
          exit(0);
        }
        else waitpid(pid2, &etat, WNOHANG);// instruction 41
      }
      
    }
    exit(0);
    
  } else  waitpid(pid, &etat, WNOHANG);// instruction 46

}