#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>

#define n1 4

int main(){

    for(int i = 0; i < n1; i++){

        if( fork() == 0 ){
            printf("Mon pid est: %d et le pid de mon père est: %d\n", getpid(), getppid());
        }
    }
    sleep(5);
}
